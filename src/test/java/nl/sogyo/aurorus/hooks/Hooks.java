package nl.sogyo.aurorus.hooks;

import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.Scenario;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import nl.sogyo.aurorus.utilities.ConfigurationReader;
import nl.sogyo.aurorus.utilities.Driver;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Logger;

public class Hooks {
    private static final SimpleDateFormat TIMESTAMP_FORMAT = new SimpleDateFormat("yyyyMMddHHmmss");
    private static final String SCREENSHOTS_DIR = "test-results/failed-scenarios/screenshots";
    private static final String FILE_EXTENSION = ".png";

    @Before(value = "@ui")
    public void setUp() {
        Driver.getDriver().get(ConfigurationReader.getProperty("BASE_URL"));
    }

    @After(value = "@ui")
    public void tearDown(Scenario scenario) {
        if (scenario.isFailed()) {
            final byte[] screenshot = ((TakesScreenshot) Driver.getDriver()).getScreenshotAs(OutputType.BYTES);
            scenario.attach(screenshot, "image/png", "screenshots");

            String scenarioName = scenario.getName();
            String screenshotPath = getScreenshot(scenarioName);
            // Use logging framework instead of println
            Logger.getLogger(Hooks.class.getName()).info("Screenshot saved at: " + screenshotPath);
//            System.out.println("Screenshot saved at: " + screenshotPath);
        }

        Driver.closeDriver();
    }

    public static String getScreenshot(String scenarioName) {
        String filename = createScreenshotFilename(scenarioName);
        Path targetPath = Paths.get(System.getProperty("user.dir"), SCREENSHOTS_DIR, filename);

        try {
            TakesScreenshot tsDriver = (TakesScreenshot) Driver.getDriver();
            try (InputStream inputStream = Files.newInputStream(tsDriver.getScreenshotAs(OutputType.FILE).toPath())) {
                Files.createDirectories(targetPath.getParent()); // Ensure the directory exists
                Files.copy(inputStream, targetPath);
            }
            return targetPath.toString();
        } catch (IOException e) {
            Logger.getLogger(Hooks.class.getName()).severe("Failed to capture screenshot: " + e.getMessage());
//            System.err.println("Failed to capture screenshot: " + e.getMessage());
            return null;
        }
    }

    private static String createScreenshotFilename(String scenarioName) {
        String sanitizedScenarioName = scenarioName.replaceAll("[^a-zA-Z0-9]", "_");
        String timestamp = TIMESTAMP_FORMAT.format(new Date());
        return "Scenario_" + sanitizedScenarioName + "_screenshot_" + timestamp + FILE_EXTENSION;
    }
}