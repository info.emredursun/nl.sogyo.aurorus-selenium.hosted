package nl.sogyo.aurorus.utilities;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public class Selenium {

    private Selenium(){}

    public static boolean isElementDisplayed(WebElement element){
        try {
            return element.isDisplayed();
        } catch (NoSuchElementException exception){
            return false;
        }
    }
}
