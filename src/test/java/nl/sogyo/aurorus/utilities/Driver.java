package nl.sogyo.aurorus.utilities;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.safari.SafariDriver;

import java.time.Duration;

public class Driver {
    private static WebDriver driver;
    private static final int implicitlyWaitSeconds = Integer.parseInt(ConfigurationReader.getProperty("implicitlyWaitSeconds"));
    private static final int explicitlyWaitSeconds = Integer.parseInt(ConfigurationReader.getProperty("explicitlyWaitSeconds"));

    public static WebDriver getDriver() {
        if (driver == null) {
            String browser = ConfigurationReader.getProperty("browser");

            switch (browser) {
                case "chrome":
                    setupChromeDriver();
                    break;
                case "firefox":
                    setupFirefoxDriver();
                    break;
                case "safari":
                    setupSafariDriver();
                    break;
                case "chrome-headless":
                    setupChromeHeadlessDriver();
                    break;
                case "firefox-headless":
                    setupFirefoxHeadlessDriver();
                    break;
                default:
                    throw new IllegalArgumentException("Invalid browser name: " + browser);
            }

            driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(implicitlyWaitSeconds));
            driver.manage().window().maximize();
        }
        return driver;
    }

    private static void setupChromeDriver() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
    }

    private static void setupFirefoxDriver() {
        WebDriverManager.firefoxdriver().setup();
        driver = new FirefoxDriver();
    }

    private static void setupSafariDriver() {
        WebDriverManager.getInstance(SafariDriver.class);
        driver = new SafariDriver();
    }

    private static void setupChromeHeadlessDriver() {
        WebDriverManager.chromedriver().setup();
        ChromeOptions options = getChromeOptions();
        driver = new ChromeDriver(options);
    }

    private static void setupFirefoxHeadlessDriver() {
        FirefoxOptions options = new FirefoxOptions();
        options.setHeadless(true);
        driver = new FirefoxDriver(options);
    }

    private static ChromeOptions getChromeOptions() {
        ChromeOptions options = new ChromeOptions();
        options.addArguments("start-maximized");
        options.addArguments("--disable-gpu");
        options.addArguments("--disable-extensions");
        options.addArguments("--proxy-server='direct://'");
        options.addArguments("--proxy-bypass-list=*");
        options.addArguments("--disable-browser-side-navigation");
        options.addArguments("--disable-dev-shm-usage");
        options.addArguments("--disable-infobars");
        options.addArguments("--no-sandbox");
        options.addArguments("enable-automation");
        options.addArguments("--window-size=1920,1200");
        options.setHeadless(true);
        return options;
    }

    public static void closeDriver() {
        if (driver != null) {
            driver.quit();
            driver = null;
        }
    }
}

