package nl.sogyo.aurorus.utilities;

import nl.sogyo.aurorus.pages.HomePage;
import nl.sogyo.aurorus.pages.LoginPage;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.junit.Test;
import org.openqa.selenium.By;

public class Log4J {
    HomePage homePage = new HomePage();
    LoginPage loginPage = new LoginPage();
    private static final String USER_EMAIL = ConfigurationReader.getProperty("userEmail");
    private static final String USER_PASSWORD = ConfigurationReader.getProperty("userPassword");
    private static final Logger logger = LogManager.getLogger(Log4J.class.getName());
    @Test
    public void log4JTest(){
        logger.info("Opening the URL");
        Driver.getDriver().get(ConfigurationReader.getProperty("BASE_URL"));
        ReusableMethods.waitForClickablility(homePage.getElementWithText("Inloggen met Google"), 15).click();
        logger.info("Clicking on Login Button");
        loginPage.email_or_phone().sendKeys(USER_EMAIL);
        loginPage.emailSubmitButton().click();
        loginPage.password().sendKeys(USER_PASSWORD);
        loginPage.passwordSubmitButton().click();
        logger.fatal("Fatal Log");//PRINTS by default with no configuration
        logger.error("Error Log");//ERROR by default with no configuration
        logger.debug("Debug Log");
        Driver.getDriver().close();
    }
}
