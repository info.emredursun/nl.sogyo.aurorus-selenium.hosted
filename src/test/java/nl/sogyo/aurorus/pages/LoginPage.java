package nl.sogyo.aurorus.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import nl.sogyo.aurorus.utilities.Driver;

public class LoginPage {

    private final WebDriver driver;

    public LoginPage(){
        driver = Driver.getDriver();
        PageFactory.initElements(driver, this);
    }

    // Locate any 'text' WebElement matching any expected 'text' value
    public WebElement getElementWithText(String elText){
        return findElementBy(By.xpath(String.format("//*[text()='%s']", elText)));
    }

    // Locate any 'text' WebElement containing any expected 'text' value
    public WebElement getElementContainsText(String elText){
        return findElementBy(By.xpath(String.format("//*[contains(text(), '%s')]", elText)));
    }

    public WebElement email_or_phone() {
        return findElementBy(By.id("identifierId"));
    }

    public WebElement password(){
        return findElementBy(By.name("Passwd"));
    }

    public WebElement emailSubmitButton(){
        return findElementBy(By.xpath("//*[@id=\"identifierNext\"]/div/button/span"));
    }
    public WebElement passwordSubmitButton(){
        return findElementBy(By.xpath("//*[@id=\"passwordNext\"]/div/button/span"));
    }

    public WebElement aurorusText(){
        return findElementBy(By.xpath("//h6[text()='Aurorus']"));
    }

    public WebElement invalidPasswordErrorMessage() {
        return findElementBy(By.xpath("//*[@id=\"yDmH0d\"]/c-wiz/div/div[2]/div/div[1]/div/form/span/section[2]/div/div/div[1]/div[2]/div[2]/span"));
    }

    public WebElement invalidEmailErrorMessage() {
        return findElementBy(By.xpath("//*[@id=\"yDmH0d\"]/c-wiz/div/div[2]/div/div[1]/div/form/span/section/div/div/div[1]/div/div[2]/div[2]/div"));
    }


    private WebElement findElementBy(By locator) {
        return driver.findElement(locator);
    }
}
