package nl.sogyo.aurorus.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import nl.sogyo.aurorus.utilities.Driver;

public class HomePage {

    private final WebDriver driver;

    public HomePage(){
        driver = Driver.getDriver();
        PageFactory.initElements(driver, this);
    }

    private WebElement findElementBy(By locator) {
        return driver.findElement(locator);
    }

    // Locate any 'text' WebElement matching any expected 'text' value
    public WebElement getElementWithText(String elText){
        return findElementBy(By.xpath(String.format("//*[text()='%s']", elText)));
    }

    public WebElement getElementWithAttributeName(String attributeName){
        return findElementBy(By.xpath(String.format("//*[@name='%s']", attributeName)));
    }

    public WebElement getElementWithAttributeNameAndValue(String attributeName, String attributeValue){
        return findElementBy(By.xpath(String.format("//*[@%s='%s']", attributeName, attributeValue)));
    }

    // Locate any 'text' WebElement containing any expected 'text' value
    public WebElement getElementContainsText(String elText){
        return findElementBy(By.xpath(String.format("//*[contains(text(), '%s')]", elText)));
    }

    public WebElement multiCollapseSection(){return findElementBy(By.className("MuiCollapse-vertical"));}

    public WebElement contractStartingDate(){return findElementBy(By.xpath("//*[@name='Ingangsdatum contract']"));}
    public WebElement contractSignedDate(){return findElementBy(By.xpath("//*[@name='Getekend op']"));}

    public WebElement monthElement() { return findElementBy(By.xpath("//select[@id='month']")); }
    public WebElement dayElement() { return findElementBy(By.xpath("//select[@id='day']")); }
    public WebElement yearElement() { return findElementBy(By.xpath("//select[@id='year']")); }

    // Welcome Message
    public WebElement welcomeTextMessage(){
        return findElementBy(By.xpath("(//*[text()='Aurorus'])[2]"));
    }


//    Nieuw en Alumni Navigation Bar
    public WebElement nieuwEnAlumniNavBar(){
        return findElementBy(By.xpath("//span[text()='Nieuw en Alumni']"));
    }

//    Recruitment Navigation Bar
    public WebElement recruitmentNavBar(){return findElementBy(By.xpath("//span[text()='Recruitment']"));}
    public WebElement benaderenSection(){
        return findElementBy(By.xpath("//span[text()='Benaderen']"));
    }

    public WebElement saveButton(){return findElementBy(By.xpath("//*[@class='css-4yld93-fabWrapper']"));}
    public WebElement closeTabButton(){return findElementBy(By.xpath("//*[@data-testid='CloseIcon']"));}

    public WebElement addButton(){
        return findElementBy(By.xpath("//*[@class='css-4yld93-fabWrapper']"));
    }

    public WebElement recenteContactenSection(){
        return findElementBy(By.xpath("//span[text()='Recente contacten']"));
    }

//    Mentoring Navigation Bar
    public WebElement mentoringNavBar(){return findElementBy(By.xpath("//span[text()='Mentoring']"));}
    public WebElement mijnTraineesSection(){
        return findElementBy(By.xpath("//span[text()='Mijn trainees']"));
    }

    public WebElement koppelTraineeMetHRSection(){
        return findElementBy(By.xpath("//span[text()='Koppel trainee met HR mentor']"));
    }

    public WebElement koppelTraineeMetAcademySection(){
        return findElementBy(By.xpath("//span[text()='Koppel trainee met academy mentor']"));
    }

//    Medewerkers Navigation Bar
    public WebElement medewerkersNavBar(){return findElementBy(By.xpath("//span[text()='Medewerkers']"));}
    public WebElement alleMedewerkersSection(){
        return findElementBy(By.xpath("//span[text()='Alle medewerkers']"));
    }

    public WebElement contractenSection(){
        return findElementBy(By.xpath("//span[text()='Contracten']"));
    }

    public WebElement gegevensControleSection(){
        return findElementBy(By.xpath("//span[text()='Gegevenscontrole']"));
    }

    public WebElement kinderenSection(){
        return findElementBy(By.xpath("//span[text()='Kinderen']"));
    }

    public WebElement alumniSection(){
        return findElementBy(By.xpath("//span[text()='Alumni']"));
    }

//    Sales Navigation Bar
    public WebElement salesNavBar(){return findElementBy(By.xpath("//span[text()='Sales']"));}
    public WebElement salesMeetingSection(){
        return findElementBy(By.xpath("//span[text()='Salesmeeting']"));
    }

    public WebElement opportunitiesSection(){
        return findElementBy(By.xpath("//span[text()='Opportunities']"));
    }

    public WebElement engineersSection(){
        return findElementBy(By.xpath("//span[text()='Engineers']"));
    }

    public WebElement klantenSection(){
        return findElementBy(By.xpath("//span[text()='Klanten']"));
    }

    public WebElement contactPersonenSection(){
        return findElementBy(By.xpath("//span[text()='Contactpersonen']"));
    }

    public WebElement lopendeOpdrachtenSection(){
        return findElementBy(By.xpath("//span[text()='Lopende opdrachten']"));
    }

//    Lopende Opdrachten / Nieuwe Opdracht
    public WebElement clientTextBox(){return findElementBy(By.xpath("(//*[contains(@class, 'MuiAutocomplete-inputFocused')])[1]"));}
    public WebElement projectType(){return findElementBy(By.xpath("//*[contains(@class, 'MuiSelect-standard')]"));}
    public WebElement startDateOfTheProject(){return findElementBy(By.xpath("//*[@name='Begin van opdracht']"));}
    public WebElement selectByVisibleText(String visibleText){return findElementBy(By.xpath(String.format("//*[text()='%s' and contains(@role, 'option')]",visibleText)));}

    public WebElement editedEntity(String entityText){return findElementBy(By.xpath(String.format("//span[text()='%s']",  entityText)));}
    public WebElement urenVerwerkenSection(){
        return findElementBy(By.xpath("//span[text()='Uren verwerken']"));
    }

//    Instellingen Navigation Bar
    public WebElement instellingenNavBar(){
        return findElementBy(By.xpath("//span[text()='Instellingen']"));
    }

}
