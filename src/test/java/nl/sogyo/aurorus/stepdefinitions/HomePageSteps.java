package nl.sogyo.aurorus.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import nl.sogyo.aurorus.pages.HomePage;
import nl.sogyo.aurorus.utilities.ConfigurationReader;
import nl.sogyo.aurorus.utilities.Driver;
import nl.sogyo.aurorus.utilities.ReusableMethods;
import org.junit.Assert;
import org.openqa.selenium.WebElement;

public class HomePageSteps {
    HomePage homePage = new HomePage();


    @Given("the user should be redirected to Aurorus {string}")
    public void the_user_should_be_redirected_to_aurorus(String navigateTo) {
        String expectedUrl = ConfigurationReader.getProperty(navigateTo);
        Driver.getDriver().get(expectedUrl);
        String actualUrl = Driver.getDriver().getCurrentUrl();
        Assert.assertEquals(expectedUrl, actualUrl);
    }

    @Then("{string} text message should be displayed")
    public void textMessageShouldBeDisplayed(String textMessage) {
        ReusableMethods.waitForVisibility(homePage.getElementWithText(textMessage), 10);
        String expectedTextMessage = "Welkom bij Aurorus!";
        String actualTextMessage = homePage.getElementWithText(textMessage).getText();
        Assert.assertEquals(expectedTextMessage, actualTextMessage);
    }

    @And("the title of the page should be {string}")
    public void theTitleOfThePageShouldBe(String expectedTitle) {
        String actualTitle = Driver.getDriver().getTitle();
        Assert.assertEquals(expectedTitle, actualTitle);
    }

    @And("the navigation menu should be visible")
    public void theNavigationMenuShouldBeVisible() {
        ReusableMethods.waitForPageToLoad(10);
        boolean nieuwEnAlumniSection = ReusableMethods.isElementDisplayed(homePage.getElementWithText("Nieuw en Alumni"));
        boolean recruitmentNavBar = ReusableMethods.isElementDisplayed(homePage.getElementWithText("Recruitment"));

        Assert.assertTrue(nieuwEnAlumniSection);
        Assert.assertTrue(recruitmentNavBar);
    }

    @Then("{string}, {string}, and {string} sections should be visible in the Navigation Bar.")
    public void and_sections_should_be_visible_in_the_navigation_bar(String section1, String section2, String section3) {
        ReusableMethods.waitForPageToLoad(10);
        boolean nieuwEnAlumniNavBar = ReusableMethods.isElementDisplayed(homePage.getElementWithText(section1));
        boolean recruitmentNavBar = ReusableMethods.isElementDisplayed(homePage.getElementWithText(section2));
        boolean instellingenNavBar = ReusableMethods.isElementDisplayed(homePage.getElementWithText(section3));

        Assert.assertTrue(nieuwEnAlumniNavBar);
        Assert.assertTrue(recruitmentNavBar);
        Assert.assertTrue(instellingenNavBar);
    }


    @Given("the user clicks on the {string} navigation bar")
    public void theUserClicksOnTheNavigationBar(String elText) {
        homePage.getElementWithText(elText).click();
        boolean desireSectionIsDisplayed = ReusableMethods.isElementDisplayed(homePage.multiCollapseSection());
       Assert.assertTrue(desireSectionIsDisplayed);
    }

    @And("the user clicks on the {string} section")
    public void theUserClicksOnTheSection(String elText) {
        homePage.getElementWithText(elText).click();
    }

    @And("the user clicks on the {string} candidate")
    public void theUserClicksOnTheCandidate(String candidate) {
        homePage.getElementWithText(candidate).click();
        boolean isCandidateDisplayed = ReusableMethods.isElementDisplayed(homePage.getElementWithText(candidate));
        Assert.assertTrue(isCandidateDisplayed);
    }

    @When("the user clicks on the Add button located in the bottom right corner")
    public void theUserClicksOnTheButtonLocatedInTheBottomRightCorner() {
        homePage.addButton().click();
    }

    @And("the user verifies that the {string} web element is displayed")
    public void theUserVerifiesThatTheWebElementIsDisplayed(String newCandidate) {
        boolean isElementDisplayed = ReusableMethods.isElementDisplayed(homePage.getElementWithText(newCandidate));
        Assert.assertTrue(isElementDisplayed);
    }

    @And("the user types the following data: {string}, {string}, {string}, {string}")
    public void theUserTypesTheFollowingData(String voorNaam, String achterNaam, String experimentCode, String geboorteDatum) {
        homePage.getElementWithAttributeName("Voornaam").sendKeys(voorNaam);
        homePage.getElementWithAttributeName("Achternaam").sendKeys(achterNaam);
        homePage.getElementWithAttributeName("Experimentcode").sendKeys(experimentCode);
        homePage.getElementWithAttributeName("Geboortedatum").sendKeys(geboorteDatum);

    }

    @And("the user saves the entered information")
    public void theUserSavesTheEnteredInformation() {
        homePage.getElementWithAttributeNameAndValue("data-testid", "SaveIcon").click();
    }

    @And("the candidate's full name should be displayed as {string}")
    public void theCandidateSFullNameShouldBeDisplayedAs(String fullName) {
        boolean isDisplayed = ReusableMethods.isElementDisplayed(homePage.getElementWithText(fullName));
        Assert.assertTrue(isDisplayed);
    }

    @Then("the user closes the current tab")
    public void theUserClosesTheCurrentTab() {
        homePage.closeTabButton().click();
    }

    @Then("the user should refresh the current page and verify that the recently saved operation is displayed as {string} on the current screen")
    public void theUserShouldRefreshTheCurrentPageAndVerifyThatTheRecentlySavedOperationIsDisplayedAsOnTheCurrentScreen(String fullName) {
        Driver.getDriver().navigate().refresh();
        WebElement fullNameEl = homePage.getElementWithText(fullName);
        boolean isDisplayed = ReusableMethods.isElementDisplayed(fullNameEl);
         while (!isDisplayed){
            Driver.getDriver().navigate().refresh();
            isDisplayed = ReusableMethods.isElementDisplayed(fullNameEl);
        }
        Assert.assertTrue(isDisplayed);
    }

    @And("the candidate's employment contract should be signed with a starting date of {string} and signed on date of {string} in the past")
    public void theCandidateSEmploymentContractShouldBeSignedWithAStartingDateOfAndSignedOnDateOfInThePast(String startingDate, String signedOnDate) {
        homePage.getElementWithAttributeName("Ingangsdatum contract").sendKeys(startingDate);
        homePage.getElementWithAttributeName("Getekend op").sendKeys(signedOnDate);
    }

    @And("the user verifies that {string} and {string} both dates are displayed correctly")
    public void theUserVerifiesThatAndBothDatesAreDisplayedCorrectly(String startingDate, String signedOnDate) {
        WebElement actualStartingDate = homePage.getElementWithAttributeName("Ingangsdatum contract");
        WebElement actualSignedOnDate = homePage.getElementWithAttributeName("Getekend op");
        ReusableMethods.waitForVisibility(actualStartingDate, 10);
        ReusableMethods.waitForVisibility(actualSignedOnDate, 10);
        Assert.assertEquals(startingDate, actualStartingDate.getText());
        Assert.assertEquals(signedOnDate, actualSignedOnDate.getText());
    }

    @And("the user types the following data {string}:{string}, {string}:{string}, {string}:{string}, {string}:{string}")
    public void theUserTypesTheFollowingData(String attName1, String attValue1, String attName2, String attValue2, String attName3, String attValue3, String attName4, String attValue4) {
        homePage.getElementWithAttributeName(attName1).sendKeys(attValue1);
        homePage.getElementWithAttributeName(attName2).sendKeys(attValue2);
        homePage.getElementWithAttributeName(attName3).sendKeys(attValue3);
        homePage.getElementWithAttributeName(attName4).sendKeys(attValue4);
    }

    @Given("the user types the following data {string}:{string}, {string}:{string}")
    public void the_user_types_the_following_data(String attName1, String attValue1, String attName2, String attValue2) {
        homePage.getElementWithAttributeName(attName1).sendKeys(attValue1);
        homePage.getElementWithAttributeName(attName2).sendKeys(attValue2);
    }

    @And("the user clicks on the web element that has the {string}:{string} feature pair")
    public void theUserClicksOnTheWebElementThatHasTheFeaturePair(String attributeName, String attributeValue) {
        homePage.getElementWithAttributeNameAndValue(attributeName, attributeValue).click();
    }

    @Then("the {string} should be created successfully")
    public void theShouldBeCreatedSuccessfully(String expectedElement) {
        Driver.getDriver().navigate().refresh();
        boolean isElementDisplayed = ReusableMethods.isElementDisplayed(homePage.getElementWithText(expectedElement));
        if (!isElementDisplayed){
            Driver.getDriver().navigate().refresh();
        }
        String actualElement = homePage.getElementWithText(expectedElement).getText();
        Assert.assertEquals(expectedElement, actualElement);

    }


    @And("the user clicks on the {string}")
    public void theUserClicksOnThe(String elementContainsText) {
        homePage.getElementContainsText(elementContainsText).click();
    }

    @And("the user fills in all the necessary details: {string}, {string}, {string}")
    public void theUserFillsInAllTheNecessaryDetails(String clientName, String projectType, String startDate) {
        homePage.clientTextBox().sendKeys(clientName);
        ReusableMethods.waitForClickablility(homePage.getElementContainsText(clientName),2).click();
        ReusableMethods.waitForClickablility(homePage.projectType(), 3).click();
        ReusableMethods.waitForClickablility(homePage.selectByVisibleText(projectType),2).click();
        homePage.startDateOfTheProject().sendKeys(startDate);
    }

    @Then("After closing the tab for the entity {string}, the user should be redirected to the engineer overview page")
    public void afterClosingTheTabForTheEntityTheUserShouldBeRedirectedToTheEngineerOverviewPage(String entityText) {
        homePage.editedEntity(entityText).click();
        boolean isDisplayed = ReusableMethods.isElementDisplayed(homePage.editedEntity(entityText));
        Assert.assertFalse(isDisplayed);
    }

    @Then("an error message {string} should be displayed")
    public void anErrorMessageShouldBeDisplayed(String errMessage) {
        boolean isDisplayed = ReusableMethods.isElementDisplayed(homePage.getElementContainsText(errMessage));
        Assert.assertTrue(isDisplayed);
    }


    @And("the candidate's employment contract {string}:{string}")
    public void theCandidateSEmploymentContract(String attributeName, String attributeValue) {
        WebElement date_field = homePage.getElementWithAttributeName(attributeName);
        date_field.click();
        ReusableMethods.waitForVisibility(date_field, 10);
        date_field.clear();
        date_field.sendKeys(attributeValue);
    }
}
