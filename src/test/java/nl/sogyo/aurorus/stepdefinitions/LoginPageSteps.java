package nl.sogyo.aurorus.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import nl.sogyo.aurorus.pages.LoginPage;
import org.junit.Assert;
import nl.sogyo.aurorus.utilities.ConfigurationReader;
import nl.sogyo.aurorus.utilities.Driver;
import nl.sogyo.aurorus.utilities.ReusableMethods;

public class LoginPageSteps {

//    String userEmail = ConfigurationReader.getProperty("userEmail");
//    String userPassword = ConfigurationReader.getProperty("userPassword");
    LoginPage loginPage = new LoginPage();

    @Given("the user is on the login page")
    public void the_user_is_on_the_login_page() {
        String expectedTitle = "Aurorus";
        String actualTitle = Driver.getDriver().getTitle();
        Assert.assertEquals(expectedTitle, actualTitle);
    }
//    @When("the user clicks on the {string} button")
//    public void the_user_clicks_on_the_button(String string) {
//        ReusableMethods.waitForClickablility(loginPage.getElementWithText(string), 15).click();
//    }
    @When("the user enters an email {string}")
    public void the_user_enters_an_email(String userEmail) {
        loginPage.email_or_phone().sendKeys(userEmail);
    }
    @When("the user clicks on the email submit button")
    public void the_user_clicks_on_the_email_submit_button() {
        loginPage.emailSubmitButton().click();
    }
    @When("the user enters a password {string}")
    public void the_user_enters_a_password(String userPassword) {
        loginPage.password().sendKeys(userPassword);
    }
    @When("the user clicks on the password submit button")
    public void the_user_clicks_on_the_password_submit_button() {
        loginPage.passwordSubmitButton().click();
    }

    @Then("the user should be redirected to Aurorus home page")
    public void the_user_should_be_redirected_to_Aurorus_home_page() {
        ReusableMethods.waitForVisibility(loginPage.aurorusText(), 10);
        String expectedUrl = ConfigurationReader.getProperty("HOME_URL");
        String actualUrl = Driver.getDriver().getCurrentUrl();
        Assert.assertEquals(expectedUrl, actualUrl);
    }

    @Then("an error message {string} should be displayed indicating invalid credentials")
    public void an_error_message_should_be_displayed_indicating_invalid_credentials(String expectedErrorMessage) {
        ReusableMethods.waitForVisibility(loginPage.getElementContainsText(expectedErrorMessage), 10);
        if (expectedErrorMessage.contains("Wrong password"))
            expectedErrorMessage = "Wrong password. Try again or click Forgot password to reset it.";
        if (expectedErrorMessage.contains("Couldn’t find")){
            expectedErrorMessage = "Couldn’t find your Google Account";
        }
        String actualErrorMessage = loginPage.getElementContainsText(expectedErrorMessage).getText();
        Assert.assertEquals(expectedErrorMessage, actualErrorMessage);
    }
}
