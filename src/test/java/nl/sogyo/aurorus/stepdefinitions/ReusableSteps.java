package nl.sogyo.aurorus.stepdefinitions;

import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import nl.sogyo.aurorus.pages.HomePage;
import org.junit.Assert;
import nl.sogyo.aurorus.pages.LoginPage;
import nl.sogyo.aurorus.utilities.ConfigurationReader;
import nl.sogyo.aurorus.utilities.ReusableMethods;

import java.io.IOException;

import static com.codeborne.selenide.Selenide.sleep;

public class ReusableSteps {
    private static final String USER_EMAIL = ConfigurationReader.getProperty("userEmail");
    private static final String USER_PASSWORD = ConfigurationReader.getProperty("userPassword");
    LoginPage loginPage = new LoginPage();
    HomePage homePage = new HomePage();

    @Given("wait for {int} seconds")
    public void i_wait_for_seconds(Integer timeForWait) {
        sleep(timeForWait * 1000); // 3 seconds = 3000 milliseconds
    }

    @Then("capture the screenshot")
    public void capture_the_screenshot() throws IOException {
        ReusableMethods.getScreenshot("capturing_screenshot");
    }

    @When("the user clicks on the {string} button")
    public void the_user_clicks_on_the_button(String elText) {
        ReusableMethods.waitForClickablility(homePage.getElementWithText(elText), 15).click();
    }

    @Then("the user clicks on the {string} sign button")
    public void theUserClicksOnTheSignButton(String attributeName) {
        ReusableMethods.waitForClickablility(homePage.getElementWithAttributeName(attributeName), 15).click();
    }

    @Then("the user confirms the message {string} on the current page")
    public void theUserConfirmsTheMessageOnTheCurrentPage(String elText) {
        ReusableMethods.waitForVisibility(homePage.getElementContainsText(elText),10);
        String textOnThePage = homePage.getElementContainsText(elText).getText();
        Assert.assertEquals(elText, textOnThePage);
    }

    @And("the user enters an email <userEmail>")
    public void theUserEntersAnEmailUserEmail() {
        loginPage.email_or_phone().sendKeys(USER_EMAIL);
    }

    @And("the user enters a password <userPassword>")
    public void theUserEntersAPasswordUserPassword() {
        loginPage.password().sendKeys(USER_PASSWORD);
    }
}
