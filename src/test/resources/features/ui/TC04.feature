@ui @setCalendar
Feature: How to select a date in a Calendar on Recruitment > Benaderen web page using Selenium

  Background: User successfully logs in with valid credentials
    Given the user is on the login page
    When the user clicks on the "Inloggen met Google" button
    And the user enters an email <userEmail>
    And the user clicks on the email submit button
    And the user enters a password <userPassword>
    And the user clicks on the password submit button
    Then the user should be redirected to Aurorus home page

  @setCalendarWithNotAcceptableDateFormat
  Scenario Outline: Setting the calendar with a not acceptable date format
    Given the user clicks on the "Recruitment" navigation bar
    And the user clicks on the "Benaderen" section
    And the candidate's full name should be displayed as "<Voornaam> <Achternaam>"
    And the user clicks on the "<Voornaam> <Achternaam>" candidate
    And the candidate's employment contract "Ingangsdatum contract":"<Ingangsdatum contract>"
    And the candidate's employment contract "Getekend op":"<Getekend op>"
    And wait for 3 seconds
    Then capture the screenshot

    Examples: Individuals who will be signing a contract with a Not Acceptable Date Format
      |Voornaam |Achternaam |Ingangsdatum contract| Getekend op |
      |Mikail   |Agca       |14-08-2023           | 21-08-2023  |

    @setCalendarWithAcceptableDateFormat
   Scenario Outline: Setting the calendar with a acceptable date format
      Given the user clicks on the "Recruitment" navigation bar
      And the user clicks on the "Benaderen" section
      And the candidate's full name should be displayed as "<Voornaam> <Achternaam>"
      And the user clicks on the "<Voornaam> <Achternaam>" candidate
      And the candidate's employment contract "Ingangsdatum contract":"<Ingangsdatum contract>"
      And the candidate's employment contract "Getekend op":"<Getekend op>"
      And wait for 3 seconds
      Then capture the screenshot

      Examples: Individuals who will be signing a contract with a Acceptable Date Format
        |Voornaam |Achternaam |Ingangsdatum contract| Getekend op |
        |Mikail   |Agca       |08/14/2023           | 08-21-2023  |