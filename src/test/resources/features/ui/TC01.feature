
@ui @loginPage
Feature: Login Functionality

  #  Scenario 1: Successful Login
  @ValidLogin
  Scenario Outline: User successfully logs in with Google credentials
    Given the user is on the login page
    When the user clicks on the "Inloggen met Google" button
    And the user enters an email "<userEmail>"
    And the user clicks on the email submit button
    And the user enters a password "<userPassword>"
    And the user clicks on the password submit button
    Then the user should be redirected to Aurorus home page
    Then capture the screenshot
#    And wait for 2 seconds

    Examples: the user valid Google credentials
      | userEmail        |userPassword  |
      | edursun@sogyo.nl |2357Sogyo*.   |

  ##  Scenario 2: Invalid Email Login Attempt
  @InvalidLogin @InvalidEmail
  Scenario Outline: User enters invalid email
    Given the user is on the login page
    When the user clicks on the "Inloggen met Google" button
    And the user enters an email "<userEmail>"
    And the user clicks on the email submit button
    Then an error message "Couldn’t find" should be displayed indicating invalid credentials
    Then capture the screenshot

    Examples: the user valid email and invalid password
      | userEmail        |userPassword  |
      | e.dursun@xyz.com |########      |

  ##  Scenario 3: Invalid Password Login Attempt
  @InvalidLogin @InvalidPassword
  Scenario Outline: User enters valid email and invalid password
    Given the user is on the login page
    When the user clicks on the "Inloggen met Google" button
    And the user enters an email "<userEmail>"
    And the user clicks on the email submit button
    And the user enters a password "<userPassword>"
    And the user clicks on the password submit button
    Then an error message "Wrong password" should be displayed indicating invalid credentials
    Then capture the screenshot

    Examples: the user valid email and invalid password
      | userEmail        |userPassword  |
      | edursun@sogyo.nl |########      |


#  ### Scenario 3: Forgot Password
#Feature: Login Functionality
#
#  Scenario: User clicks on "Forgot Password" link
#    Given the user is on the login page
#    When the user clicks on the "Forgot Password" link
#    Then the user should be redirected to the password reset page
#
#  Scenario: User requests password reset
#    Given the user is on the password reset page
#    When the user enters their registered email "user@example.com"
#    And the user clicks the "Reset Password" button
#    Then a success message should be displayed indicating that a reset email has been sent