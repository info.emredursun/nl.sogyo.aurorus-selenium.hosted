# Scenario: Recruitment Validation
@ui @recruitment
Feature: Recruitment Functionality

  Background: User successfully logs in with valid credentials
    Given the user is on the login page
    When the user clicks on the "Inloggen met Google" button
    And the user enters an email <userEmail>
    And the user clicks on the email submit button
    And the user enters a password <userPassword>
    And the user clicks on the password submit button
    Then the user should be redirected to Aurorus home page

  @addNewCandidate
  Scenario Outline: Add a new candidate via Recruitment > Approach
    Given the user clicks on the "Recruitment" navigation bar
    And the user clicks on the "Benaderen" section
    And the user clicks on the Add button located in the bottom right corner
    And the user verifies that the "Nieuwe kandidaat" web element is displayed
    And the user types the following data "Voornaam":"<Voornaam>", "Achternaam":"<Achternaam>", "Experimentcode":"<Experimentcode>", "Geboortedatum":"<Geboortedatum>"
    And the user saves the entered information
    And the candidate's full name should be displayed as "<Voornaam> <Achternaam>"
    And the user closes the current tab
    Then the user should refresh the current page and verify that the recently saved operation is displayed as "<Voornaam> <Achternaam>" on the current screen

    Examples: new candidate data
      |Voornaam |Achternaam    |Experimentcode|Geboortedatum|
      |James    |Tigger        |TC03          |01/11/1987 |


  @signContract
  Scenario Outline: Sign employment contract for a new candidate
    Given the user clicks on the "Recruitment" navigation bar
    And the user clicks on the "Benaderen" section
    And the candidate's full name should be displayed as "<Voornaam> <Achternaam>"
    And the user clicks on the "<Voornaam> <Achternaam>" candidate
    And the candidate's employment contract "Ingangsdatum contract":"<Ingangsdatum contract>"
    And the candidate's employment contract "Getekend op":"<Getekend op>"
    And the user clicks on the "getekend" sign button
    And the user should refresh the current page and verify that the recently saved operation is displayed as "<Voornaam> <Achternaam>" on the current screen
    And the user clicks on the "<Voornaam> <Achternaam>" candidate
    Then the user confirms the message "Arbeidsovereenkomst getekend" on the current page

    Examples: Individuals who will be signing a contract
      |Voornaam |Achternaam |Ingangsdatum contract| Getekend op |
      |James    |Tigger     |08/14/2023           | 08-21-2023  |

  @addNewCustomer
  Scenario Outline: Add a new customer via Sales > Customers
    Given the user clicks on the "Sales" navigation bar
    And the user clicks on the "Klanten" section
    And the user clicks on the Add button located in the bottom right corner
    And the user types the following data "Naam":"<Naam>", "E-mailadres":"<E-mailadres>"
    And the user clicks on the web element that has the "data-testid":"SaveIcon" feature pair
    And the user closes the current tab
    Then the "<Naam>" should be created successfully

    Examples: Customers who will be added
      |Naam            |E-mailadres                       |
      |Bank of America |administration@bankofamerica.nl   |


  @createTask
  Scenario Outline: Create new task for candidate and customer
    Given the user clicks on the "Sales" navigation bar
    And the user clicks on the "Engineers" section
    And the user clicks on the "<Voornaam> <Achternaam>" candidate
    And the user clicks on the "Nieuwe opdracht aanmaken"
    And the user fills in all the necessary details: "<Klant>", "<Soort opdracht>", "<Begin van opdracht>"
    And the user clicks on the "Opdracht opslaan"
    And the user closes the current tab
    And After closing the tab for the entity "<Voornaam> <Achternaam>", the user should be redirected to the engineer overview page
    And an error message "Niet alle wijzigingen zijn opgeslagen" should be displayed
    Then capture the screenshot
#    And wait for 5 seconds

    Examples: Candidates who were hired
      |Voornaam |Achternaam |Klant            |Soort opdracht  |Begin van opdracht|
      |James    |Tigger     |Bank of America  |Project         | 08-23-2023       |



#  You can test the following (slightly more complex) scenario with the Aurorus Selenium environment. As mentioned, this is a real bug.
#
#  1. Add a new candidate via Recruitment > Approach
#  2. Have the candidate sign an employment contract (choose a date in the past so that the candidate is immediately employed)
#  3. Also add a new customer via Sales > Customers
#  4. Under Sales > Engineers, go to the candidate you just hired
#  5. Select "Create New Task" and fill in all the necessary details (you can use the customer you created in step 3)
#  6. Save the new task
#  7. Close the overview of this engineer using the cross in the top bar
#
#  Desired effect: You return to the engineer overview
#  Actual effect: You get a pop-up warning you of unsaved changes (which don't exist)