@ui @homePage
Feature: Home Page Functionality

  Background: User successfully logs in with valid credentials
    Given the user is on the login page
    When the user clicks on the "Inloggen met Google" button
    And the user enters an email <userEmail>
    And the user clicks on the email submit button
    And the user enters a password <userPassword>
    And the user clicks on the password submit button
    Then the user should be redirected to Aurorus home page


  @welcomeMessage
  Scenario: Verify "Welkom bij Aurorus!" text message on the BASE_URL page
    Given the user should be redirected to Aurorus "BASE_URL"
    And the title of the page should be "Aurorus"
    Then "Welkom bij Aurorus!" text message should be displayed

  @navBar
  Scenario: Verify the appearance of the homepage
    Given the user should be redirected to Aurorus "HOME_URL"
    And the navigation menu should be visible
    Then "Nieuw en Alumni", "Recruitment", and "Instellingen" sections should be visible in the Navigation Bar.