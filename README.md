# Test Automation [WEB]

##### UI Automation Testing Using Java, Selenium, Gherkin, Cucumber

### Environment and specs:
- [aurorus](https://aurorus-selenium.hosted.sogyo.nl/)

### dependencies:
- [cucumber-java v7.11.0](https://mvnrepository.com/artifact/io.cucumber/cucumber-java)
- [cucumber-junit v7.11.0](https://mvnrepository.com/artifact/io.cucumber/cucumber-junit)
- [hamcrest v2.2](https://mvnrepository.com/artifact/org.hamcrest/hamcrest)
- [assertj v3.24.2](https://mvnrepository.com/artifact/org.assertj/assertj-core)
- [jackson v2.14.1](https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind)
- [selenide v6.17.0](https://mvnrepository.com/artifact/com.codeborne/selenide/6.17.0)

### plugins:
- [maven-failsafe-plugin v3.0.0-M8](https://mvnrepository.com/artifact/org.apache.maven.plugins/maven-failsafe-plugin)

### Setting Up
These instructions will get you a copy of the project up and running on your local machine.

- *GitHub Clone Repository:*
```shell
git clone https://github.com/emredursun/nl.sogyo.aurorus-selenium.hosted.git
```
- *GitLab Clone Repository:*
```shell
git clone https://gitlab.com/info.emredursun/nl.sogyo.aurorus-selenium.hosted.git
```
- *set project sdk as 17*

Running tests from terminal:
```shell
mvn -B verify --file pom.xml
```
Running tests in CI/CD pipeline:
- In this project I used gitlab CI.

### About scenarios:
### <b>Automated scenarios are:</b>
#### UI
- Login Functionality
- Home Page Functionality
- Recruitment Functionality
- Sales Functionality

What's next?
- 
